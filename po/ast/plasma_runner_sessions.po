# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023, 2024 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-06 01:40+0000\n"
"PO-Revision-Date: 2024-03-15 01:17+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.0\n"

#: sessionrunner.cpp:20
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to log out of the "
"session"
msgid "logout;log out"
msgstr "zarrar la sesión;zarru de la sesión"

#: sessionrunner.cpp:23
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr ""

#: sessionrunner.cpp:26
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to shut down the "
"computer"
msgid "shutdown;shut down;power;power off"
msgstr "apagar;apagáu;enerxía"

#: sessionrunner.cpp:29
#, kde-format
msgid "Turns off the computer"
msgstr "Apaga l'ordenador"

#: sessionrunner.cpp:32
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to restart the "
"computer"
msgid "restart;reboot"
msgstr "reaniciar;reaniciu"

#: sessionrunner.cpp:35
#, kde-format
msgid "Reboots the computer"
msgstr "Reanicia l'ordenador"

#: sessionrunner.cpp:39
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to lock the screen"
msgid "lock;lock screen"
msgstr "bloquiar;bloquéu;bloquéu de la pantalla;bloquéu de pantalla"

#: sessionrunner.cpp:41
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Bloquia les sesiones actuales y anicia'l curiapantalles"

#: sessionrunner.cpp:44
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to save the "
"desktop session"
msgid "save;save session"
msgstr ""
"guardar la sesión;guardáu de la sesión;guardáu de sesiones;guardáu de les "
"sesiones"

#: sessionrunner.cpp:47
#, kde-format
msgid "Saves the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:50
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to switch user "
"sessions"
msgid "switch user;new session"
msgstr "cambiar d'usuariu;cambéu d'usuariu;sesión nueva"

#: sessionrunner.cpp:53
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Anicia una sesión nueva con otru usuariu"

#: sessionrunner.cpp:56
#, kde-format
msgctxt "KRunner keyword to list user sessions"
msgid "sessions"
msgstr "sesiones"

#: sessionrunner.cpp:57
#, kde-format
msgid "Lists all sessions"
msgstr "Llista toles sesiones"

#: sessionrunner.cpp:60
#, kde-format
msgctxt "KRunner keyword to switch user sessions"
msgid "switch"
msgstr "cambiar;cambéu"

#: sessionrunner.cpp:62
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""

#: sessionrunner.cpp:81
#, kde-format
msgctxt "log out command"
msgid "Log Out"
msgstr "Zarrar la sesión"

#: sessionrunner.cpp:89
#, kde-format
msgctxt "turn off computer command"
msgid "Shut Down"
msgstr "Apagar"

#: sessionrunner.cpp:96
#, kde-format
msgctxt "restart computer command"
msgid "Restart"
msgstr "Reaniciar"

#: sessionrunner.cpp:103
#, kde-format
msgctxt "lock screen command"
msgid "Lock"
msgstr "Bloquiar"

#: sessionrunner.cpp:110
#, kde-format
msgid "Save Session"
msgstr "Guardar la sesión"

#: sessionrunner.cpp:151
#, kde-format
msgid "Switch User"
msgstr ""

#: sessionrunner.cpp:237
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type '%1')</li><li>Plasma widgets (such as the "
"application launcher)</li></ul>"
msgstr ""

#: sessionrunner.cpp:246
#, kde-format
msgid "New Desktop Session"
msgstr ""

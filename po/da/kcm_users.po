# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Martin Schlander <mschlander@opensuse.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-25 00:40+0000\n"
"PO-Revision-Date: 2020-11-17 19:41+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: src/fingerprintmodel.cpp:137 src/fingerprintmodel.cpp:221
#, kde-format
msgid "No fingerprint device found."
msgstr ""

#: src/fingerprintmodel.cpp:294
#, kde-format
msgid "Retry scanning your finger."
msgstr ""

#: src/fingerprintmodel.cpp:296
#, kde-format
msgid "Swipe too short. Try again."
msgstr ""

#: src/fingerprintmodel.cpp:298
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr ""

#: src/fingerprintmodel.cpp:300
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr ""

#: src/fingerprintmodel.cpp:308
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr ""

#: src/fingerprintmodel.cpp:311
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""

#: src/fingerprintmodel.cpp:314
#, kde-format
msgid "The device was disconnected."
msgstr ""

#: src/fingerprintmodel.cpp:319
#, kde-format
msgid "An unknown error has occurred."
msgstr ""

#: src/ui/ChangePassword.qml:27 src/ui/UserDetailsPage.qml:171
#, kde-format
msgid "Change Password"
msgstr "Skift adgangskode"

#: src/ui/ChangePassword.qml:32
#, kde-format
msgid "Set Password"
msgstr "Angiv adgangskode"

#: src/ui/ChangePassword.qml:55
#, kde-format
msgid "Password"
msgstr "Adgangskode"

#: src/ui/ChangePassword.qml:70
#, kde-format
msgid "Confirm password"
msgstr "Bekræft adgangskode"

#: src/ui/ChangePassword.qml:89 src/ui/CreateUser.qml:68
#, kde-format
msgid "Passwords must match"
msgstr "Adgangskoderne skal matche"

#: src/ui/ChangeWalletPassword.qml:16
#, fuzzy, kde-format
#| msgid "Change Password"
msgid "Change Wallet Password?"
msgstr "Skift adgangskode"

#: src/ui/ChangeWalletPassword.qml:25
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""

#: src/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr ""

#: src/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""

#: src/ui/ChangeWalletPassword.qml:58
#, fuzzy, kde-format
#| msgid "Change Password"
msgid "Change Wallet Password"
msgstr "Skift adgangskode"

#: src/ui/ChangeWalletPassword.qml:67
#, kde-format
msgid "Leave Unchanged"
msgstr ""

#: src/ui/CreateUser.qml:15
#, kde-format
msgid "Create User"
msgstr "Opret bruger"

#: src/ui/CreateUser.qml:33 src/ui/UserDetailsPage.qml:133
#, kde-format
msgid "Name:"
msgstr "Navn:"

#: src/ui/CreateUser.qml:37 src/ui/UserDetailsPage.qml:141
#, kde-format
msgid "Username:"
msgstr "Brugernavn:"

#: src/ui/CreateUser.qml:47 src/ui/UserDetailsPage.qml:151
#, kde-format
msgid "Standard"
msgstr "Standard"

#: src/ui/CreateUser.qml:48 src/ui/UserDetailsPage.qml:152
#, kde-format
msgid "Administrator"
msgstr "Administrator"

#: src/ui/CreateUser.qml:51 src/ui/UserDetailsPage.qml:155
#, kde-format
msgid "Account type:"
msgstr "Kontotype:"

#: src/ui/CreateUser.qml:56
#, kde-format
msgid "Password:"
msgstr "Adgangskode:"

#: src/ui/CreateUser.qml:61
#, kde-format
msgid "Confirm password:"
msgstr "Bekræft adgangskode:"

#: src/ui/CreateUser.qml:78
#, kde-format
msgid "Create"
msgstr "Opret"

#: src/ui/EnrollFeedback.qml:27
#, kde-format
msgid "Enrolling Fingerprint"
msgstr ""

#: src/ui/EnrollFeedback.qml:37
#, kde-format
msgid ""
"Please repeatedly press your right index finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:39
#, kde-format
msgid ""
"Please repeatedly press your right middle finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:41
#, kde-format
msgid ""
"Please repeatedly press your right ring finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:43
#, kde-format
msgid ""
"Please repeatedly press your right little finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:45
#, kde-format
msgid "Please repeatedly press your right thumb on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:47
#, kde-format
msgid ""
"Please repeatedly press your left index finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:49
#, kde-format
msgid ""
"Please repeatedly press your left middle finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:51
#, kde-format
msgid ""
"Please repeatedly press your left ring finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:53
#, kde-format
msgid ""
"Please repeatedly press your left little finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:55
#, kde-format
msgid "Please repeatedly press your left thumb on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:59
#, kde-format
msgid ""
"Please repeatedly swipe your right index finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:61
#, kde-format
msgid ""
"Please repeatedly swipe your right middle finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:63
#, kde-format
msgid ""
"Please repeatedly swipe your right ring finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:65
#, kde-format
msgid ""
"Please repeatedly swipe your right little finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:67
#, kde-format
msgid "Please repeatedly swipe your right thumb on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:69
#, kde-format
msgid ""
"Please repeatedly swipe your left index finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:71
#, kde-format
msgid ""
"Please repeatedly swipe your left middle finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:73
#, kde-format
msgid ""
"Please repeatedly swipe your left ring finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:75
#, kde-format
msgid ""
"Please repeatedly swipe your left little finger on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:77
#, kde-format
msgid "Please repeatedly swipe your left thumb on the fingerprint sensor."
msgstr ""

#: src/ui/EnrollFeedback.qml:93
#, kde-format
msgid "Finger Enrolled"
msgstr ""

#: src/ui/FingerprintDialog.qml:28
#, kde-format
msgid "Configure Fingerprints"
msgstr ""

#: src/ui/FingerprintDialog.qml:40
#, kde-format
msgid "Add"
msgstr ""

#: src/ui/FingerprintDialog.qml:52
#, kde-format
msgid "Cancel"
msgstr ""

#: src/ui/FingerprintDialog.qml:60
#, kde-format
msgid "Done"
msgstr ""

#: src/ui/FingerprintList.qml:46
#, kde-format
msgid "Re-enroll finger"
msgstr ""

#: src/ui/FingerprintList.qml:58
#, fuzzy, kde-format
#| msgid "Delete files"
msgid "Delete fingerprint"
msgstr "Slet filer"

#: src/ui/FingerprintList.qml:74
#, kde-format
msgid "No fingerprints added"
msgstr ""

#: src/ui/main.qml:20
#, kde-format
msgid "Users"
msgstr ""

#: src/ui/main.qml:31
#, fuzzy, kde-format
#| msgid "Add New User"
msgctxt "@action:button As in, 'add new user'"
msgid "Add New"
msgstr "Tilføj en ny bruger"

#: src/ui/main.qml:107
#, kde-format
msgctxt "@info:usagetip"
msgid "Press Space to edit the user profile"
msgstr ""

#: src/ui/PickFinger.qml:28
#, kde-format
msgid "Pick a finger to enroll"
msgstr ""

#: src/ui/PicturesSheet.qml:18
#, fuzzy, kde-format
#| msgid "Change Avatar"
msgctxt "@title"
msgid "Change Avatar"
msgstr "Skift avatar"

#: src/ui/PicturesSheet.qml:22
#, kde-format
msgctxt "@item:intable"
msgid "It's Nothing"
msgstr ""

#: src/ui/PicturesSheet.qml:23
#, fuzzy, kde-format
#| msgid "Feisty Flamingo"
msgctxt "@item:intable"
msgid "Feisty Flamingo"
msgstr "Feisty Flamingo"

#: src/ui/PicturesSheet.qml:24
#, fuzzy, kde-format
#| msgid "Dragon's Fruit"
msgctxt "@item:intable"
msgid "Dragon's Fruit"
msgstr "Dragon's Fruit"

#: src/ui/PicturesSheet.qml:25
#, fuzzy, kde-format
#| msgid "Sweet Potato"
msgctxt "@item:intable"
msgid "Sweet Potato"
msgstr "Sweet Potato"

#: src/ui/PicturesSheet.qml:26
#, fuzzy, kde-format
#| msgid "Ambient Amber"
msgctxt "@item:intable"
msgid "Ambient Amber"
msgstr "Ambient Amber"

#: src/ui/PicturesSheet.qml:27
#, fuzzy, kde-format
#| msgid "Sparkle Sunbeam"
msgctxt "@item:intable"
msgid "Sparkle Sunbeam"
msgstr "Sparkle Sunbeam"

#: src/ui/PicturesSheet.qml:28
#, fuzzy, kde-format
#| msgid "Lemon-Lime"
msgctxt "@item:intable"
msgid "Lemon-Lime"
msgstr "Lemon-Lime"

#: src/ui/PicturesSheet.qml:29
#, fuzzy, kde-format
#| msgid "Verdant Charm"
msgctxt "@item:intable"
msgid "Verdant Charm"
msgstr "Verdant Charm"

#: src/ui/PicturesSheet.qml:30
#, fuzzy, kde-format
#| msgid "Mellow Meadow"
msgctxt "@item:intable"
msgid "Mellow Meadow"
msgstr "Mellow Meadow"

#: src/ui/PicturesSheet.qml:31
#, fuzzy, kde-format
#| msgid "Tepid Teal"
msgctxt "@item:intable"
msgid "Tepid Teal"
msgstr "Tepid Teal"

#: src/ui/PicturesSheet.qml:32
#, fuzzy, kde-format
#| msgid "Plasma Blue"
msgctxt "@item:intable"
msgid "Plasma Blue"
msgstr "Plasma Blue"

#: src/ui/PicturesSheet.qml:33
#, fuzzy, kde-format
#| msgid "Pon Purple"
msgctxt "@item:intable"
msgid "Pon Purple"
msgstr "Pon Purple"

#: src/ui/PicturesSheet.qml:34
#, fuzzy, kde-format
#| msgid "Bajo Purple"
msgctxt "@item:intable"
msgid "Bajo Purple"
msgstr "Bajo Purple"

#: src/ui/PicturesSheet.qml:35
#, fuzzy, kde-format
#| msgid "Burnt Charcoal"
msgctxt "@item:intable"
msgid "Burnt Charcoal"
msgstr "Burnt Charcoal"

#: src/ui/PicturesSheet.qml:36
#, fuzzy, kde-format
#| msgid "Paper Perfection"
msgctxt "@item:intable"
msgid "Paper Perfection"
msgstr "Paper Perfection"

#: src/ui/PicturesSheet.qml:37
#, fuzzy, kde-format
#| msgid "Cafétera Brown"
msgctxt "@item:intable"
msgid "Cafétera Brown"
msgstr "Cafétera Brown"

#: src/ui/PicturesSheet.qml:38
#, fuzzy, kde-format
#| msgid "Rich Hardwood"
msgctxt "@item:intable"
msgid "Rich Hardwood"
msgstr "Rich Hardwood"

#: src/ui/PicturesSheet.qml:60
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr ""

#: src/ui/PicturesSheet.qml:78
#, kde-format
msgctxt "@action:button"
msgid "Initials"
msgstr ""

#: src/ui/PicturesSheet.qml:132
#, fuzzy, kde-format
#| msgid "Choose File..."
msgctxt "@action:button"
msgid "Choose File…"
msgstr "Vælg fil..."

#: src/ui/PicturesSheet.qml:137
#, fuzzy, kde-format
#| msgid "Choose a picture"
msgctxt "@title"
msgid "Choose a picture"
msgstr "Vælg et billede"

#: src/ui/PicturesSheet.qml:185
#, kde-format
msgctxt "@action:button"
msgid "Placeholder Icon"
msgstr ""

#: src/ui/PicturesSheet.qml:274
#, kde-format
msgctxt "@info:whatsthis"
msgid "User avatar placeholder icon"
msgstr ""

#: src/ui/UserDetailsPage.qml:112
#, fuzzy, kde-format
#| msgid "Change Avatar"
msgid "Change avatar"
msgstr "Skift avatar"

#: src/ui/UserDetailsPage.qml:165
#, kde-format
msgid "Email address:"
msgstr "E-mailadresse:"

#: src/ui/UserDetailsPage.qml:194
#, kde-format
msgid "Delete files"
msgstr "Slet filer"

#: src/ui/UserDetailsPage.qml:201
#, kde-format
msgid "Keep files"
msgstr "Behold filer"

#: src/ui/UserDetailsPage.qml:208
#, fuzzy, kde-format
#| msgid "Delete User..."
msgid "Delete User…"
msgstr "Slet bruger..."

#: src/ui/UserDetailsPage.qml:220
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr ""

#: src/ui/UserDetailsPage.qml:252
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""

#: src/user.cpp:290
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Kunne ikke få rettighed til at gemme brugeren %1"

#: src/user.cpp:295
#, kde-format
msgid "There was an error while saving changes"
msgstr "Der opstod en fejl under gemning af ændringerne"

#: src/user.cpp:394
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""

#: src/user.cpp:402
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr ""

#: src/usermodel.cpp:146
#, kde-format
msgid "Your Account"
msgstr "Din konto"

#: src/usermodel.cpp:146
#, kde-format
msgid "Other Accounts"
msgstr "Andre konti"

#~ msgid "Manage Users"
#~ msgstr "Håndtér brugere"

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "jens.hansen@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Martin Schlander"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mschlander@opensuse.org"

#~ msgid "Manage user accounts"
#~ msgstr "Håndtér brugerkonti"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "Jens Hansen"
